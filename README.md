# Frenchy's Dotfiles

To use run:
```
curl -L https://gitlab.com/bgiov/frenchys-dotfiles/raw/master/dotfile-setup/init.sh | zsh
```

Now open iterm and import the profile in `dotfile-setup/iterm.json

That should be everything!
