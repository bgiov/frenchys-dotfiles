" Turn off filetype detection during install
filetype off
set nocompatible

" Initialise the pathogen vim plugin handler
set rtp+=~/.vim/bundle/vundle
call vundle#begin()
Plugin 'VundleVim/Vundle.vim', {'name': 'vundle'}

" Setup plugins
Plugin 'ntpeters/vim-better-whitespace'
Plugin 'othree/yajs.vim'
Plugin 'stephpy/vim-yaml'
Plugin 'mkitt/tabline.vim'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'fatih/vim-go'
Plugin 'kien/ctrlp.vim'
Plugin 'othree/es.next.syntax.vim'
Plugin 'rhysd/vim-crystal'
Plugin 'Valloric/YouCompleteMe'
Plugin 'vim-syntastic/syntastic'
Plugin 'nvie/vim-flake8'
Plugin 'tpope/vim-fugitive'
Plugin 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}

" Make python code pretty
let python_highlight_all=1

" You complete me customizations
let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g :YcmCompleter GoToDefinitionElseDeclaration<CR>

" Make vim aware of virtualenv
python3 << EOF
import os
import sys
if 'VIRTUAL_ENV' in os.environ:
    project_base_dir = os.environ['VIRTUAL_ENV']
    activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
    exec(compile(open(activate_this, 'rb').read(), activate_this, 'exec'), dict(__file__=activate_this))
EOF

" End Setup
call vundle#end()
filetype plugin indent on

" Turn on syntax highlighting
syntax on

" Set netrw (the inbuilt file browser) with some nicer settings
let g:netrw_liststyle=3
let g:netrw_list_hide= '.*\.sw[a-z]$,.*\.pyc$,__pycache__\/'
let g:netrw_banner=0

" Turn on line numbering (absolute + relative hybrid)
set number rnu

" Setup nice defaults for tabbing and indentation
set backspace=indent,eol,start tabstop=4 shiftwidth=4 nocindent expandtab smartindent

" Turn on the statusbar
set laststatus=2

" Fix the tmux issue with colors
set t_Co=256

" Map space the "leader" keyboard shortcut
let mapleader=" "
map <leader>k :E<cr>

" No Bad Habits Matey (disable arrows)
noremap  <Up> ""
noremap! <Up> <Esc>
noremap  <Down> ""
noremap! <Down> <Esc>
noremap  <Left> ""
noremap! <Left> <Esc>
noremap  <Right> ""
noremap! <Right> <Esc>
inoremap jk <esc>

" Code folding
set fdm=indent

" Don't store swap files in the directory
set directory=$HOME/.vim/swapfiles//

" Set javascript specific niceness for indentation
autocmd Filetype javascript setlocal ts=2 sts=2 sw=2 expandtab foldmethod=syntax
