#!/bin/zsh
git clone --bare https://gitlab.com/jaitaiwan/dans-dotfiles.git $HOME/.dotfiles
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
dotfiles checkout
dotfiles config --local status.showUntrackedFiles no
dotfiles submodule init
dotfiles submodule update
~/.zsh/fonts/install.sh
echo "Setup done, simply run 'zsh' to start or if its already running, restart it.\n"
echo "Protip: Use 'chsh -s /bin/zsh' to use zsh as your terminal default"
